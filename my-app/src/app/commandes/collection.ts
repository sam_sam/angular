import {Commandes} from './interfaces/Commandes.model';

export const COLLECTION: Commandes[] = [
  {
    name: 'sam sam',
    reference: '0101',
    state: 0
  },
  {
    name: 'mas mas',
    reference: '1010',
    state: 1
  },
  {
    name: 'msa msa',
    reference: '1001',
    state: 2
  }

];
