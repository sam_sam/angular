import { Component, OnInit } from '@angular/core';
import { Commandes } from '../../interfaces/commandes.model';
import { COLLECTION } from '../../collection';

@Component({
  selector: 'app-list-commandes',
  templateUrl: './list-commandes.component.html',
  styleUrls: ['./list-commandes.component.scss']
})
export class ListCommandesComponent implements OnInit {
  collection: Commandes[] = COLLECTION;

  constructor() { }

  ngOnInit() {
  }

}
