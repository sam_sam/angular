import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ListCommandesComponent } from './containers/list-commandes/list-commandes.component';


const commandeRoutes: Routes = [{ path: 'list', component: ListCommandesComponent }];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(commandeRoutes)
  ],
  declarations: []
})
export class CommandeRoutingModule { }
