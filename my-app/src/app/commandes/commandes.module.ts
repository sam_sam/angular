import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { CommandeRoutingModule } from './commande-routing.module';

import { ListCommandesComponent } from './containers/list-commandes/list-commandes.component';
import { CommandeComponent } from './component/commande/commande.component';



@NgModule({
  imports: [CommonModule, SharedModule, CommandeRoutingModule ],
  declarations: [ListCommandesComponent, CommandeComponent],
  exports: [ListCommandesComponent]

})
export class CommandesModule { }
