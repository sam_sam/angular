import { Component, OnInit, Input } from '@angular/core';
import { State } from '../../enums/state.enum';
import { Commandes } from '../../interfaces/commandes.model';

@Component({
  selector: 'app-commande',
  templateUrl: './commande.component.html',
  styleUrls: ['./commande.component.scss']
})
export class CommandeComponent implements OnInit {
  @Input('commandes') commandes: Commandes;
  state= State;
  constructor() { }

  ngOnInit() {
  }

  changeState(commandes: Commandes, newState: State): void {commandes.state = newState;
    console.log(commandes);
    }



}
